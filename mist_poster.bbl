\begin{thebibliography}{1}

\bibitem{carrico_illustration_2006}
J.~A. Carri\c{c}o, C.~Silva-Costa, J.~Melo-Cristino, F.~R. Pinto,
  H.~de~Lencastre, J.~S. Almeida, and M.~Ramirez.
\newblock Illustration of a common framework for relating multiple typing
  methods by application to macrolide-resistant \emph{{Streptococcus
  pyogenes}}.
\newblock {\em J. Clin. Microbiol.}, 44(7):2524--2532, July 2006.

\bibitem{carrillo_framework_2012}
Catherine~D. Carrillo, Peter Kruczkiewicz, Steven Mutschall, Andrei Tudor,
  Clifford Clark, and Eduardo~N. Taboada.
\newblock A framework for assessing the concordance of molecular typing methods
  and the true strain phylogeny of \emph{{Campylobacter jejuni}} and \emph{{C.
  coli}} using draft genome sequence data.
\newblock {\em Frontiers in Cellular and Infection Microbiology}, 2, May 2012.
\newblock {PMID:} 22919648 {PMCID:} {PMC3417556}.

\bibitem{severiano_evaluation_2011}
Ana Severiano, Jo\~ao~A. Carri\c{c}o, D.~Ashley Robinson, M\'ario Ramirez, and
  Francisco~R. Pinto.
\newblock Evaluation of jackknife and bootstrap for defining confidence
  intervals for pairwise agreement measures.
\newblock {\em {PLoS} {ONE}}, 6(5):e19539, May 2011.

\bibitem{severiano_adjusted_2011}
Ana Severiano, Francisco~R. Pinto, M\'ario Ramirez, and Jo\~ao~A. Carri\c{c}o.
\newblock Adjusted wallace as a measure of congruence between typing methods.
\newblock {\em Journal of Clinical Microbiology}, September 2011.

\end{thebibliography}
