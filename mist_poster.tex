\documentclass[final,hyperref={pdfpagelabels=false}]{beamer}
\usepackage{grffile}
\mode<presentation>{\usetheme{I6pd2}}
\usepackage[english]{babel}
\usepackage[latin1]{inputenc}
\usepackage{amsmath,amsthm, amssymb, latexsym}
%\usepackage{times}\usefonttheme{professionalfonts}  % obsolete
%\usefonttheme[onlymath]{serif}
\boldmath
\usepackage[orientation=portrait,size=a0,scale=1.4,debug]{beamerposter}
% change list indention level
% \setdefaultleftmargin{3em}{}{}{}{}{}


%\usepackage{snapshot} % will write a .dep file with all dependencies, allows for easy bundling
\usepackage{caption}
\usepackage{bibentry}
\usepackage{array,booktabs,tabularx}
\newcolumntype{Z}{>{\centering\arraybackslash}X} % centered tabularx columns
\newcommand{\pphantom}{\textcolor{ta3aluminium}} % phantom introduces a vertical space in p formatted table columns??!!
\captionsetup{labelformat=empty}
\listfiles

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\graphicspath{{figures/}}
 
\title{MIST: a tool for rapid \textit{in silico} generation of molecular data from bacterial genome sequences}
\author{Peter Kruczkiewicz$^{1,2}$,
    Steven Mutschall$^{1}$,
    Dillon Barker$^{1}$,
    James Thomas$^{2}$,
    Gary Van Domselaar$^{4}$,
    Victor P.J. Gannon$^{1}$,
    Catherine D. Carrillo$^{3}$
    and Eduardo N. Taboada$^{1}$}
\institute{Laboratory for Foodborne Zoonoses, Public Health Agency of Canada$^{1}$, 
    University of Lethbridge$^{2}$, 
  Bureau of Microbial Hazards, Health Canada$^{3}$,
    National Microbiology Laboratory, Public Health Agency of Canada$^{4}$}
\date[Feb. 10, 2013]{Feb. 10, 2013}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\newlength{\columnheight}
\setlength{\columnheight}{104cm}


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\begin{document}
\begin{frame}
  \begin{columns}
    % ---------------------------------------------------------%
    % Set up a column 
    \begin{column}{.5\textwidth}
      \begin{beamercolorbox}[center,wd=\textwidth]{postercolumn}
        \begin{minipage}[T]{.95\textwidth}  % tweaks the width, makes a new \textwidth
          \parbox[t][\columnheight]{\textwidth}{ % must be some better way to set the the height, width and textwidth simultaneously
            % Since all columns are the same length, it is all nice and tidy.  You have to get the height empirically
            % ---------------------------------------------------------%
            % fill each column with content            
            \begin{block}{Introduction}
              \begin{itemize}
                \item An ever-expanding number of microbial genomes are being sequenced.
                \item How can we use this whole-genome sequence (WGS) data to
                \begin{itemize}
                  \item contextualize WGS data with respect to historical typing data?
                  \item assess current typing methods?
                  \item design novel typing methods?
                \end{itemize}
                \item \textit{In silico} generation of typing data from WGS data can enable these analyses
              \end{itemize}
            \end{block}

            \vfill

            \begin{block}{MIST: in silico generation of typing data}
              \begin{center}
                \includegraphics[width=.45\linewidth]{MIST_overview_flowchart}
                ~\hspace{50pt}
                \includegraphics[width=.4\linewidth]{mist_main}
              \end{center}
              \vspace{50pt}
              \begin{itemize}
                \item Input draft or completed WGS multifasta
                \item Select \textit{in silico} typing assays
                \item Generate \textit{in silico} typing data
              \end{itemize}

            \end{block}
            
            \vfill

            \begin{block}{Framework for optimized typing methods}
              \begin{center}
                \includegraphics[width=.55\linewidth]{optimized_typing_method_flowchart}
              \end{center}
              
              \begin{itemize}
                % \item Identify potential molecular markers
                % \item Select markers and create \textit{in silico} assays
                % \item Generate \textit{in silico} typing data using MIST
                % \item Compare methods against phylogenetic data 
                % \item Assess performance of typing methods 
                \item We will illustrate this approach on \textit{Listeria monocytogenes}
              \end{itemize}
            \end{block}
            \vfill
            \begin{block}{\small Phylogenomic clusters: a gold-standard for molecular typing assessment?}
              \begin{center}
                \includegraphics[width=1.0\linewidth]{heatmap_snp_log10_to_clusters}
              \end{center}
              \begin{itemize}
                \item Determine core genome phylogeny from concatenome of core genes
                \item Determine pairwise SNP counts from core genome concatenome
                \item Define \textbf{phylogenomic clusters} based on SNP thresholds to allow assessment of typing methods against core genome phylogeny \cite{carrillo_framework_2012}
              \end{itemize}
            \end{block}
          }
          % ---------------------------------------------------------%
          % end the column
        \end{minipage}
      \end{beamercolorbox}
    \end{column}
    % ---------------------------------------------------------%
    % end the column


        % ---------------------------------------------------------%
    % Set up a column 
    \begin{column}{.5\textwidth}
      \begin{beamercolorbox}[center,wd=\textwidth]{postercolumn}
        \begin{minipage}[T]{.95\textwidth} % tweaks the width, makes a new \textwidth
          \parbox[t][\columnheight]{\textwidth}{ % must be some better way to set the the height, width and textwidth simultaneously
            % Since all columns are the same length, it is all nice and tidy.  You have to get the height empirically
            % ---------------------------------------------------------%
            % fill each column with content

            \begin{block}{Typing assay design and optimization}
              \begin{center}
                \includegraphics[width=0.95\linewidth]{cgf_assay_design_process}
              \end{center}
              \begin{itemize}
                \item Workflow for designing a PCR-based comparative genomic fingerprinting (CGF) assay
                \begin{itemize}
                  \item A) Defined pool of 169 suitable CGF markers 
                  \item B) Randomly generated and assessed concordance of 25 million possible 40 marker CGF assays to the phylogenomic clusters
                  \item C) Potential CGF assays with low to high concordance to the phylogenomic clusters 
                \end{itemize}
              \end{itemize}
            \end{block}

            \vfill

            \begin{block}{Typing method assessment}
              \begin{table}
                \small\centering
                \renewcommand{\arraystretch}{1.1}
                \begin{tabular}{@{}llcllllll@{}} 
                \toprule
                 & & & & & & & \multicolumn{2}{c}{20 SNP clusters} \\
                 \cmidrule(lr){7-9}
                Method & & Partitions & & $D$ & CI & & $AW$ & CI \\
                 \midrule
                Novel CGF & & 35 & & 0.940 & (0.892-0.987) & & 1.000 & (1.000-1.000) \\
                MLST & & 22 & & 0.927 & (0.895-0.958) & & 0.710 & (0.478-0.945)\\
                MLVST & & 22 & & 0.876 & (0.808-0.944) & & 0.772 & (0.559-1.000)\\
                \bottomrule
                \end{tabular}
                \caption{
                Simpson's index of diversity ($D$) and Adjusted Wallace ($AW$) with 95\% confidence intervals (CI) are shown here for MLST, MVLST and the novel CGF assay compared to phylogenomic clusters (PGC) defined at 20 SNPs.
                $D$ and $AW$ values with 95\% CI were calculated using the online tool ComparingPartitions \cite{carrico_illustration_2006,severiano_adjusted_2011}.
                }
              \end{table}
              
              \vspace{10pt}

              \begin{table} [!htb]
                \small\centering
                \renewcommand{\arraystretch}{1.1}
                \begin{tabular}{@{}llllllllllr@{} }
                \toprule
                \multicolumn{4}{l}{$AW_{(\text{Method A}\rightarrow{}\text{20 SNP clusters})}$}  & & \multicolumn{4}{l}{$AW_{(\text{Method B}\rightarrow{}\text{20 SNP clusters})}$} \\
                \cmidrule(lr){1-4}\cmidrule(r){6-9}
                Method A & & AW & CI & & Method B & & AW & CI &  & $P$\\ 
                \midrule
                MLST & & 0.710 & (0.478-0.945) &  & MVLST & & 0.772 & (0.559-1.000)  & & 0.197\\ 
                Novel CGF & & 1.000 & (1.000-1.000) & & MVLST & & 0.772 & (0.559-1.000) &  & *0.046\\ 
                Novel CGF & & 1.000 & (1.000-1.000) & & MLST & & 0.710 & (0.478-0.945) &  & *0.013\\ 
                \bottomrule
                \end{tabular}
                \caption{
                The $P$-values between $AW$ of different typing methods to the phylogenomic clusters defined at 20 SNPs are shown here.
                $P$ were calculated using the jackknife pseudo-values resampling method with the online tool ComparingPartitions \cite{carrico_illustration_2006,severiano_adjusted_2011}.
                % *$P \leq$ 0.05
                }
              \end{table}

              \vspace{20pt}

              \begin{itemize}
                \item Compared to the MLST and MVLST methods, novel CGF assay has 
                \begin{itemize}
                  \item greater discriminatory power
                  \item greater concordance with phylogenomic clusters
                \end{itemize}
              \end{itemize}
            \end{block}

            \vfill

            \begin{block}{Conclusions}
              \begin{itemize}
                \item \textit{In silico} typing of WGS can facilitate
                \begin{itemize}
                  \item comparison of typing methods against each other
                  \item comparison of typing methods against a core genome phylogeny
                  \item the design and assessment of novel typing methods
                  \item rapid analysis and contextualization of newly sequenced microbial isolates with regards to historical typing data
                \end{itemize}
              \end{itemize}
            \end{block}

            \vfill

            \begin{block}{Acknowledgements}
              \scriptsize
              This research was funded by the Government of Canada's Genomics Research and Development Initiative.
              The authors would also like to acknowledge the University of Lethbridge for partial support of PK.
            \end{block}

            \vfill

            \begin{block}{References}
              \tiny
              \setbeamertemplate{bibliography item}[text]
              \setbeamercolor{bibliography item}{fg=black}
              \setbeamercolor{bibliography entry author}{fg=black}
              \setbeamercolor{bibliography entry title}{fg=black}
              \setbeamercolor{bibliography entry location}{fg=black}
              \setbeamercolor{bibliography entry note}{fg=black}
              \begin{thebibliography}{1}

                \bibitem{carrico_illustration_2006}
                {Carri\c{c}o, J. A. and Silva-Costa, C. and Melo-Cristino, J. and Pinto, F. R. and de Lencastre, H. and Almeida, J. S. and Ramirez, M.}, \emph{Illustration of a Common Framework for Relating Multiple Typing Methods by Application to Macrolide-Resistant \emph{{Streptococcus pyogenes}}}, {J. Clin. Microbiol.}, {2006}

                \bibitem{carrillo_framework_2012}
                Carrillo, Catherine D. and Kruczkiewicz, Peter and Mutschall, Steven and Tudor, Andrei and Clark, Clifford and Taboada, Eduardo N., \emph{A Framework for Assessing the Concordance of Molecular Typing Methods and the True Strain Phylogeny of {\emph{{Campylobacter jejuni}}} and {\emph{{C. coli}}} Using Draft Genome Sequence Data}, {Frontiers in Cellular and Infection Microbiology}, {2012}

                \bibitem{severiano_adjusted_2011}
                {Severiano, Ana and Pinto, Francisco R. and Ramirez, M\'ario and Carri\c{c}o, Jo\~ao A.}, \emph{Adjusted Wallace as a Measure of Congruence between Typing Methods}, {Journal of Clinical Microbiology}, {2011}

                % \bibitem{severiano_evaluation_2011}
                % {Severiano, Ana and Carri\c{c}o, Jo\~ao A. and Robinson, D. Ashley and Ramirez, M\'ario and Pinto, Francisco R.}, \emph{Evaluation of Jackknife and Bootstrap for Defining Confidence Intervals for Pairwise Agreement Measures}, {{PLoS} {ONE}}, {2011}
              \end{thebibliography}
              % \bibliographystyle{plain}
              % \bibliography{poster_references}
            \end{block}
          }
          % ---------------------------------------------------------%
          % end the column
        \end{minipage}
      \end{beamercolorbox}
    \end{column}
    % ---------------------------------------------------------%
    % end the column
  \end{columns}
  \vskip1ex
  %\tiny\hfill\textcolor{ta2gray}{Created with \LaTeX \texttt{beamerposter}  \url{http://www-i6.informatik.rwth-aachen.de/~dreuw/latexbeamerposter.php}}
  \tiny\hfill{Created with \LaTeX \texttt{beamerposter}  \url{http://www-i6.informatik.rwth-aachen.de/~dreuw/latexbeamerposter.php} \hskip1em}
\end{frame}
\end{document}


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%% Local Variables: 
%%% mode: latex
%%% TeX-PDF-mode: t
%%% End:
